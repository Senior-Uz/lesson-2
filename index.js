// 1. Write a program that is divisible by 3 and 5 <br>
console.log("First Task");

function devison(number) {
  for (let i = 1; i < number; i++) {
    if (i % 3 == 0 && i % 5 == 0) {
      console.log(i);
    }
  }
}
devison(100);

// 2. Write a program that checks whether given number odd or even.
console.log("Second Task");

function oddOrEven(numbers) {
  if (numbers % 2 == 0) {
    console.log("even");
  } else {
    console.log("odd");
  }
}

oddOrEven(531);

//3. Write a program that get array as argument and sorts it by order // [1,2,3,4...]<br>
console.log("Third  Task");

let sortingNumbers = [5, 3, 56, 876, 23, 87, -232, 0, -2, 32, 19];
let sorted = sortingNumbers.sort((a, b) => {
  if (a > b) return 1;
  if (a < b) return -1;
});

console.log(sorted);

// we can do it another ways
// let sorted = sortingNumbers.sort((a, b) => a - b);
// console.log(sorted);

////////////////////////////////
// 4. Write a program that return unique set of array:
console.log("Fourth task");

function printingUnique(arr, n) {
  // Picking all elements one by one
  for (let i = 0; i < n; i++) {
    // Stop looping if the picked element is already printed.
  }
}

// I tried to do the last task but my brain had already stopped thinking 🤯 🤯 🤯 ,
